# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="A POP3 server written entirely in Perl"
HOMEPAGE="http://kiza.kcore.de/software/snowbox/"
SRC_URI="http://kiza.kcore.de/software/snowbox/download/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="virtual/perl-Digest-MD5"
RDEPEND="$DEPEND"

src_unpack(){
	unpack $A
	cd $S
	epatch ${FILESDIR}/${PV}-Makefile.patch
}

src_install(){
	emake install DESTDIR="${D}" || die "make install failed!!!"
}
