# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit libtool eutils

MY_P="pcre-${PV}"
MY_PN="pcre"
MY_PV="${PV}"

DESCRIPTION="Perl-compatible regular expression library"
HOMEPAGE="http://www.pcre.org/"
SRC_URI="ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/${MY_P}.tar.bz2"

LICENSE="BSD"
SLOT="3"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc-macos ~ppc64 ~s390 ~sh ~sparc ~sparc-fbsd ~x86 ~x86-fbsd"
IUSE="doc unicode static"

DEPEND="dev-util/pkgconfig"
RDEPEND=""

S="${WORKDIR}/${MY_P}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	epatch "${FILESDIR}"/pcre-6.3-uclibc-tuple.patch
	epatch "${FILESDIR}"/pcre-6.4-link.patch

	# Added for bug #130668 -- fix parallel builds
	epatch "${FILESDIR}"/pcre-6.6-parallel-build.patch

	elibtoolize
}

src_compile() {
	
	if use unicode; then
		myconf="--enable-utf8 --enable-unicode-properties"
	fi

	econf ${myconf} $(use_enable static) || die "econf failed"
	emake all libpcrecpp.la || die "emake failed"
}

src_install() {
	make DESTDIR="${D}" install || die "make install failed"

	dodoc doc/*.txt doc/Tech.Notes AUTHORS
	use doc && dohtml doc/html/*
}
