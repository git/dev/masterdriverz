# Copyright 1999-2006 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $

RESTRICT="nomirror"

K_PREPATCHED="yes"
K_NOSETEXTRAVERSION="don't_set_it"

ETYPE="sources"
inherit kernel-2 eutils
detect_version

RAD_SRC="http://no.oldos.org/files/${KV}/${KV}.bz2"
RAD_SRC="${RAD_SRC} http://joecool.ftfuchs.com/no-sources/${KV}/${KV}.bz2"

DESCRIPTION="Development branch of the Linux Kernel. Has several performance-ish patches, and some more."
HOMEPAGE="http://no.oldos.org/"

SRC_URI="${KERNEL_URI} ${RAD_SRC}"

UNIPATCH_STRICTORDER="yes"
UNIPATCH_LIST="${DISTDIR}/${KV}.bz2"

KEYWORDS="x86 amd64 ia64 -*"

pkg_postinst() {
        postinst_sources

        einfo "Rad Sources - Performance Patchset:"
        einfo "by: cheater-conrad (Brandon) - cheater1034@gmail.com"
        echo
        einfo "Please report all bugs to cheater-conrad, no one else!"
        einfo "if you are having a problem, try a vanilla kernel first,"
	einfo "if the problem persists, then we may assist you"
}

