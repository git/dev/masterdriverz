# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

UNIPATCH_LIST="${DISTDIR}/linux-${KV}.bz2"
K_PREPATCHED="yes"
K_NOUSENAME="yes"
UNIPATCH_STRICTORDER="yes"

ETYPE="sources"
inherit eutils kernel-2
detect_version

DESCRIPTION="Viper-Sources, the other ever evolving kernel patchset.  My playground for new patches."
HOMEPAGE="http://evolution-mission.org"

KEYWORDS="~x86 ~amd64 ~ppc ~ppc64"
RESTRICT="nomirror"

SRC_URI="${KERNEL_URI}
        http://vipernicus.evolution-mission.org/patches/${OKV}/${EXTRAVERSION/-}/linux-${KV}.bz2"

src_install() {
	kernel-2_src_install

	into /usr
	dobin ${FILESDIR}/emission-tunables
}

pkg_postinst() {
        postinst_sources

	einfo "Viper Sources brought to you by:"
	einfo "Cory Grunden <cory.grunden@gmail.com> http://vipernicus.evolution-mission.org"
	einfo "Alex Heck <nesl247@gmail.com> http://evolution-mission.org"
	echo
	einfo "Please report all bugs to our forums: http://evolution-mission.org"
	einfo "We make no guarantee that emission-sources is completely bug-free."
}
