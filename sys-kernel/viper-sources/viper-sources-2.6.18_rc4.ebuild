# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

ETYPE="sources"
RESTRICT="nomirror"
BRANCH_REVISION="1"
BRANCH_NAME="Ah, Now That Hits the Spot"

inherit kernel-2
detect_version

UNIPATCH_LIST="${DISTDIR}/linux-${KV}${BRANCH_REVISION}.bz2"
UNIPATCH_STRICTORDER="yes"
IUSE=""

DESCRIPTION="Viper Sources is pretty much my playground for new patches.

The main goal of this patchset is to offer latest features, decrease latencies, decrease overhead, and improve interactivity. The only difference is experimental versions of everything. I boot test each release and run for an extended amount of time."
	
HOMEPAGE="http://vipernicus.evolution-mission.org/"
SRC_URI="${KERNEL_URI}
	 http://vipernicus.evolution-mission.org/patches/${KV_MAJOR}.${KV_MINOR}.${KV_PATCH}${RELEASE}/viper${BRANCH_REVISION}/linux-${KV}${BRANCH_REVISION}.bz2"

KEYWORDS="-* ~x86 ~amd64 ~ppc"

pkg_postinst() {
	postinst_sources

	einfo "Warning:"
	einfo "Viper Sources was packaged in a factory that packages peanuts. If you are allergic"
	einfo "to peanuts, then you are gambling with your life."
	ewarn ""
	ewarn "I, Vipernicus, and the only person you should have to deal with in relation with this patchset."
	ewarn "In other words, leave those poor Gentoo Devs alone, they bite."
	echo
	einfo "Well, it's emerged now, get to building. ;)"
	echo
}
