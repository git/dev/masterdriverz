# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Beyond Kernel Sources -- successor to arch, tiger, nitro"
HOMEPAGE="http://iphitus.loudas.com/beyond.html"
KEYWORDS="~x86"
DEPEND=""
RDEPEND=""

K_NOUSENAME="yes"
K_EXTRAEINFO="\
The beyond stable tree is a patchset based on the CK patchset. It aims to
include a variety of popular features and updates that have not yet made
it to the vanilla kernel, while remaining relatively stable. Con Kolivas'
CK patchset is the most important part of beyond stable tree, as it
provides the great interactivity and responsiveness improvements that it
is noted for.

Features
-ck(s) Con Kolivas Patchset, (server version available as option) -ide
libATA/ide updates, Alsa updates and fixes, Dothan Speedstep, Pentium M
undervolt, IBM ACPI fan control, Suspend2, vesafb-tng, reiser4, unionfs
squashfs, realtime-lsm, fbsplash, configurable mouse polling support,
custom dsdt, Layer7, various fixes and updates."

ETYPE="sources"
inherit kernel-2
#detect_version

MY_N=${PN/-sources/}
# A few hacks to set beyond version via _p instead of -r
# _p#[#] is mandatory
MY_P=${PV/*_p/}
MY_P=${MY_P/-r*/}
# now see if version is dotted (2 chars)
# strip off rightmost 0
if [ ${MY_P: -1} = "0" ] ; then
	MY_P=${MY_P:0:1}
fi
# if length is not 1, then add a dot
if [ ${#MY_P} -ne "1" ] ; then
	MY_P=${MY_P:0:1}.${MY_P:1}
fi
# accomodate revisions in patchset
if [ ${PR} != "r0" ] ; then
	MY_PR=${PR/r/-r}
else
	MY_PR=""
fi	
EXTRAVERSION=-${MY_N}${MY_P}${MY_PR}
KV_FULL=${OKV}${EXTRAVERSION}
KV_CK=${KV_FULL/-r*/}

detect_version

CK_PATCH="patch-${KV_CK}.bz2"
UNIPATCH_LIST="${DISTDIR}/${CK_PATCH}"

SRC_URI="${KERNEL_URI}
	http://iphitus.loudas.com/beyond/${OKV}/${KV_CK}/${CK_PATCH}"

pkg_postinst() {
	postinst_sources

	einfo "The beyond sources are based on the -ck patchset."
	einfo "The ck-server patches are integrated, and are selectable"
	einfo "during the config process."
}
