# Copyright 2006-2006 BreakMyGentoo.org
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# no kernel.eclass
# portions lifted from eclass
# needed to permit _pre versions of beyond to emerge
# with proper kernel version.

inherit eutils versionator

DESCRIPTION="Beyond Kernel Sources -- successor to arch, tiger, nitro"
HOMEPAGE="http://iphitus.loudas.com/beyond.html"
RESTRICT="binchecks strip primaryuri"

# ripped from kernel-2.eclass
KV_MAJOR=$(get_version_component_range 1 ${PV})
KV_MINOR=$(get_version_component_range 2 ${PV})
KV_PATCH=$(get_version_component_range 3 ${PV})
KV_FULL=${KV_MAJOR}.${KV_MINOR}.${KV_PATCH}

# get beyond version
B_MAJOR=$(get_version_component_range 4 ${PV})
# separate beyond major.minor
B_MINOR=${B_MAJOR: -1}
B_MAJOR=${B_MAJOR:0:1}
# create dotted version if needed
if [ ${B_MINOR} -ne 0 ]
then
	B_VERSION=${B_MAJOR}.${B_MINOR}
else
	B_VERSION=${B_MAJOR}
fi
# create base beyond name
K_EXTRAVERSION="beyond${B_VERSION}"
#determine if a PRE release
if [[ ${PV} == *_pre* ]]
then
	K_EXTRAVERSION=${K_EXTRAVERSION}pre${PV: -1}
fi
# check for revision
if [ "${PR}" != "r0" ]
then
	K_EXTRAVERSION=${K_EXTRAVERSION}-${PR}
fi
# shortcut for later
K_NAME=${KV_FULL}-${K_EXTRAVERSION}
# get rid of PRE. Maybe one time hack
P_NAME=${K_NAME/pre1/}

# however, full path to file includes PRE
SRC_URI="mirror://kernel/linux/kernel/v${KV_MAJOR}.${KV_MINOR}/linux-${KV_FULL}.tar.bz2
	http://iphitus.loudas.com/beyond/${KV_FULL}/${K_NAME}/patch-${P_NAME}.bz2"
LICENSE="GPL-2"
SLOT="${K_NAME}"
KEYWORDS="~amd64 ~x86"
PROVIDE="virtual/linux-sources virtual/alsa"
IUSE=""

DEPEND="sys-apps/sed"
RDEPEND=""

src_unpack() {
	unpack linux-${KV_FULL}.tar.bz2
	cd ${WORKDIR}/linux-${KV_FULL}
	epatch ${DISTDIR}/patch-${P_NAME}.bz2
}

src_install() {
	dodir /usr/src/
	mv ${WORKDIR}/linux-${KV_FULL} ${D}/usr/src/linux-${K_NAME}
}
