# Copyright 1999-2006 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Only set MMV to numeric value of -mm release
# Version of Andrew Morton's patchset

RESTRICT="nomirror"

K_PREPATCHED="yes"
K_NOSETEXTRAVERSION="don't_set_it"

ETYPE="sources"
inherit kernel-2 eutils
detect_version

# Create -mm patch version, -mm patch source, -no patch source
MMV="1"
MMPV="${KV/-no*/}-mm${MMV}"
MMPV_SRC="mirror://kernel/linux/kernel/people/akpm/patches/2.6/${MMPV/-mm*/}/${MMPV}/${MMPV}.bz2" 

# add more as you go along
NO_SRC="http://no.oldos.org/files/${KV}/${KV}.bz2"

DESCRIPTION="Development branch of the Linux Kernel with Andrew Morton's patchset and other performance-ish patches and tweaks. Maintained by joecool and jasonf.  Hosted by linode.com"
HOMEPAGE="http://no.oldos.org/"

SRC_URI="${KERNEL_URI} ${MMPV_SRC}  ${NO_SRC}"

UNIPATCH_STRICTORDER="yes"
UNIPATCH_LIST="${DISTDIR}/${MMPV}.bz2 ${DISTDIR}/${KV}.bz2"

KEYWORDS="x86 ~amd64 ~ia64 -*"

K_EXTRAEWARN="IMPORTANT: cheater-conrad, joecool, and jasonF would like to
remind you that this is an experimental kernel still.  Don't blame us
if things break. If you experience problems while using no-sources,
go back to a vanilla kernel and see if the problem persists. If the problems
are still there, then you can bug somebody. (Preferably us and
not the Gentoo devs!) We lurk at #no-sources on Freenode.

No-Sources was founed by joecool and jasonF and is currently
maintained by cheater-conrad. For help on installing and more information
please visit http://gentoo-wiki.com/HOWTO_no-sources."
