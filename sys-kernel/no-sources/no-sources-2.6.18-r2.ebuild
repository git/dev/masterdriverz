# Copyright 1999-2006 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Only set MMV to numeric value of -mm release
# Version of Andrew Morton's patchset

RESTRICT="nomirror"

K_PREPATCHED="yes"
K_NOSETEXTRAVERSION="don't_set_it"

ETYPE="sources"
inherit kernel-2 eutils
detect_version

# Create -mm patch version, -mm patch source, -no patch source
MMV="3"
MMPV="${KV/-no*/}-mm${MMV}"
MMPV_SRC="mirror://kernel/linux/kernel/people/akpm/patches/2.6/${MMPV/-mm*/}/${MMPV}/${MMPV}.bz2" 

# add more as you go along
NO_SRC="http://no.oldos.org/files/${KV}/${KV}.bz2"

DESCRIPTION="Development branch of the Linux Kernel with Andrew Morton's patchset and other performance-ish patches and tweaks. Maintained by joecool and jasonf.  Hosted by linode.com"
HOMEPAGE="http://no.oldos.org/"

SRC_URI="${KERNEL_URI} ${MMPV_SRC}  ${NO_SRC}"

UNIPATCH_STRICTORDER="yes"
UNIPATCH_LIST="${DISTDIR}/${MMPV}.bz2 ${DISTDIR}/${KV}.bz2"

KEYWORDS="x86 ~amd64 ~ia64 -*"

pkg_postinst() {
        postinst_sources

        einfo "IMPORTANT: cheater-conrad, joecool, and jasonF would like to remind you that"
        einfo "this is an experimental kernel. Do not blame us if things break. If you experience"
        einfo "problems while using no-sources, go back to a vanilla kernel and see if the problem"
        einfo "persists. If the problem persists, please bug someone (Us and NOT the gentoo devs!"
        einfo "--- We lurk at #no-sources on Freenode, if no one's there try #conrad ---"
        echo
        einfo "Read the Notes.txt BEFORE using this patch!"
        echo
        einfo "No-Sources was founded by joecool and jasonF, and it is currently maintained by"
        einfo "cheater-conrad. For help on installing and for more information, please visit:"
        einfo "http://gentoo-wiki.com/HOWTO_no-sources"
	echo
}
