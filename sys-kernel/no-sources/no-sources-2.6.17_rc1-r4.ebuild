# Copyright 1999-2006 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Only set MMV to numeric value of -mm release
# Version of Andrew Morton's patchset

RESTRICT="nomirror"

K_PREPATCHED="yes"
K_NOSETEXTRAVERSION="don't_set_it"

ETYPE="sources"
inherit kernel-2 eutils
detect_version

MMV="3"
MMPV="${KV/-no*/}-mm${MMV}"
MMPV_SRC="mirror://kernel/linux/kernel/people/akpm/patches/2.6/${MMPV/-mm*/}/${MMPV}/${MMPV}.bz2"

NOPV_SRC="http://no.oldos.org/files/${KV}/${KV}.bz2 http://joecool.ftfuchs.com/no-sources/${KV}/${KV}.bz2"

DESCRIPTION="Development branch of the Linux Kernel with Andrew Morton's patchset and other performance-ish patches and tweaks. Maintained by cheater-conrad and joecool. Hosted by jasonf and linode.com"
HOMEPAGE="http://no.oldos.org/"
SRC_URI="${KERNEL_URI} ${MMPV_SRC} ${NOPV_SRC}"

UNIPATCH_STRICTORDER="yes"
EPATCH_OPTS="-p1"
UNIPATCH_LIST="${DISTDIR}/${MMPV}.bz2 ${DISTDIR}/${KV}.bz2"
PATCH_DEPTH=1

KEYWORDS="x86 ~amd64 ~ia64 -*"

K_EXTRAEWARN="IMPORTANT: cheater-conrad, joecool, predatorfreak, and jasonf would like to 
remind you that this is an experimental kernel still.  Don't blame us
if things break. If you experience problems while using no-sources,
go back to a vanilla kernel and see if the problem persists. If the problems
are still there, then you can bug somebody. (Preferably us and
not the Gentoo devs!) We lurk at #no-sources on Freenode."
