# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

K_PREPATCHED="yes"
K_NOUSENAME="yes"
UNIPATCH_STRICTORDER="yes"

ETYPE="sources"
inherit eutils kernel-2
detect_version

DESCRIPTION="Emission-Sources, the ever evolving kernel patchset."
HOMEPAGE="http://evolution-mission.org"

IUSE="minimum no-lockless"
KEYWORDS="~x86 ~amd64 ~ppc ~ppc64"
RESTRICT="nomirror"

if use minimum; then
UNIPATCH_LIST="${DISTDIR}/linux-${KV}-minimum.bz2"
else
UNIPATCH_LIST="${DISTDIR}/linux-${KV}.bz2"
fi

SRC_URI="${KERNEL_URI}
        minimum? ( http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/linux-${KV}-minimum.bz2 )
        !minimum? ( http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/linux-${KV}.bz2 )
	no-lockless? ( http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/revert-lockless.patch )"

# Only patch if flag enabled
useq no-lockless && UNIPATCH_LIST="${UNIPATCH_LIST} ${DISTDIR}/revert-lockless.patch"

pkg_preinst() {
	einfo "Press CTRL + C now and enable the minimum USE flag if you want"
	einfo "a barebones patch, that does not include: reiser4, suspend2, splash, etc"
	epause 5
}

pkg_postinst() {
        postinst_sources

	einfo "Evolution Mission Sources brought to you by:"
	einfo "Cory Grunden <cory.grunden@gmail.com> http://vipernicus.evolution-mission.org"
	einfo "Alex Heck <nesl247@gmail.com> http://evolution-mission.org"
	einfo "Brett G.	<predatorfreak@dcaf-security.org> http://dcaf-security.org"
	echo
	einfo "Please report all bugs to our forums: http://evolution-mission.org"
	einfo "We make no gurantee that emission-sources is completely bug-free."
}
