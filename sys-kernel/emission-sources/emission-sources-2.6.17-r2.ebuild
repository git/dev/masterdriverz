# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:  $

UNIPATCH_LIST="${DISTDIR}/linux-${KV}.bz2"
K_PREPATCHED="yes"
K_NOUSENAME="yes"
UNIPATCH_STRICTORDER="yes"

ETYPE="sources"
inherit eutils kernel-2
detect_version

DESCRIPTION="Emission-Sources, the ever evolving kernel patchset."
HOMEPAGE="http://evolution-mission.org"

IUSE="staircase no-lockless"
KEYWORDS="~x86 ~amd64 ~ppc ~ppc64"
RESTRICT="nomirror"

SRC_URI="${KERNEL_URI} http://distfiles.evolution-mission.org/${PN}/${OKV}/${EXTRAVERSION/-}/linux-${KV}.bz2
	staircase? ( http://distfiles.evolution-mission.org/${PN}/${OKV}/${EXTRAVERSION/-}/linux-${KV}-staircase.patch )
	no-lockless? ( http://distfiles.evolution-mission.org/${PN}/${OKV}/${EXTRAVERSION/-}/linux-${KV}-nolockless.patch )"

# Only patch if flag enabled
useq staircase && UNIPATCH_LIST="${UNIPATCH_LIST} ${DISTDIR}/linux-${KV}-staircase.patch"
useq no-lockless && UNIPATCH_LIST="${UNIPATCH_LIST} ${DISTDIR}/linux-${KV}-nolockless.patch"

pkg_preinst() {
	einfo "Press CTRL + C now and enable the staircase USE flag if you want to"
	einfo "use staircase \(default in the old archck and beyond patchsets\) instead of ignosched"
	epause 5
}

pkg_postinst() {
        postinst_sources

	einfo "Evolution Mission Sources brought to you by:"
	einfo "Cory Grunden <cory.grunden@gmail.com> http://vipernicus.evolution-mission.org"
	einfo "Alex Heck    <nesl247@gmail.com>      http://evolution-mission.org"
	echo
	einfo "Please report all bugs to our bugzilla: http://bugzilla.evolution-mission.org"
	einfo "We make no gurantee that emission-sources is completely bug-free."
}