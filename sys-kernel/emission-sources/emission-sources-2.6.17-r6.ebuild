# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

UNIPATCH_LIST="${DISTDIR}/linux-${KV}.bz2"
K_PREPATCHED="yes"
K_NOUSENAME="yes"
UNIPATCH_STRICTORDER="yes"

ETYPE="sources"
inherit eutils kernel-2
detect_version

DESCRIPTION="Emission-Sources, the ever evolving kernel patchset."
HOMEPAGE="http://evolution-mission.org"

IUSE="lockless"
KEYWORDS="~x86 ~amd64 ~ppc ~ppc64"
RESTRICT="nomirror"

SRC_URI="${KERNEL_URI}
        http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/linux-${KV}.bz2
	lockless? ( http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/linux-${KV}-lockless.patch )"

# Only patch if flag enabled
useq lockless && UNIPATCH_LIST="${UNIPATCH_LIST} ${DISTDIR}/linux-${KV}-lockless.patch"

pkg_preinst() {
	einfo "Press CTRL + C now and enable the lockless USE flag if you"
	einfo "want to use lockless."
	epause 5
}

pkg_postinst() {
        postinst_sources

	einfo "Evolution Mission Sources brought to you by:"
	einfo "Cory Grunden <cory.grunden@gmail.com> http://vipernicus.evolution-mission.org"
	einfo "Alex Heck <nesl247@gmail.com> http://evolution-mission.org"
	echo
	einfo "Please report all bugs to our forums: http://evolution-mission.org"
	einfo "We make no guarantee that emission-sources is completely bug-free."
}
