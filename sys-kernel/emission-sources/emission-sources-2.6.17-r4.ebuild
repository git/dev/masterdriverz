# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:  $

UNIPATCH_LIST="${DISTDIR}/patch-${KV}.patch.bz2"
K_PREPATCHED="yes"
K_NOUSENAME="yes"
UNIPATCH_STRICTORDER="yes"

ETYPE="sources"
inherit eutils kernel-2
detect_version

DESCRIPTION="Emission-Sources, the ever evolving kernel patchset."
HOMEPAGE="http://evolution-mission.org"

IUSE="no-suspend2 no-lockless"
KEYWORDS="~x86 ~amd64 ~ppc ~ppc64"
RESTRICT="nomirror"

SRC_URI="${KERNEL_URI}
        http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/patch-${KV}.patch.bz2
	no-suspend2? ( http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/revert-suspend2.patch )
	no-lockless? ( http://distfiles.evolution-mission.org/sources/${OKV}/${EXTRAVERSION/-}/revert-lockless.patch )"

# Only patch if flag enabled
useq no-suspend2 && UNIPATCH_LIST="${UNIPATCH_LIST} ${DISTDIR}/revert-suspend2.patch"
useq no-lockless && UNIPATCH_LIST="${UNIPATCH_LIST} ${DISTDIR}/revert-lockless.patch"

pkg_preinst() {
	einfo "Press CTRL + C now and enable the no-suspend2 USE flag if you"
	einfo "do not want to use suspend2."
	epause 5
}

pkg_postinst() {
        postinst_sources

	einfo "Evolution Mission Sources brought to you by:"
	einfo "Cory Grunden <cory.grunden@gmail.com> http://vipernicus.evolution-mission.org"
	einfo "Alex Heck <nesl247@gmail.com> http://evolution-mission.org"
	einfo "Brett G.	<predatorfreak@dcaf-security.org> http://dcaf-security.org"
	echo
	einfo "Please report all bugs to our forums: http://evolution-mission.org"
	einfo "We make no gurantee that emission-sources is completely bug-free."
}